Features:
Foundation 5
SASS
HTML5 CSS3
Full Responsive
Parallax Background
1200px Grid Based
Social Links
Multi-Browser Support
Clean code
Well commented code
Documented
Premium Slider
*.PSD file with everything included, layered and named
Install Grunt
Opent the folder in your terminal and run the next command:

npm install && npm install -g bower && npm install -g grunt-cli && bower install

After write "grunt" and hit enter to start the grunt.

After this all css and JavaScript files will be compiled.

HTML Structure
This theme is a fluid layout with two columns and It is built on Foundation Framework.

Below is a file structure:

- - [ assets

- - - - [ fonts

- - - - [ images

- - - - [ javascript

- - - - [ vendor js

- - [ src

- - - - [ sass

- - - - - - [ layout

- - - - - - [ modules

- - [ 404.html

- - [ contact.html

- - [ doctors.html

- - [ index.html

- - [ news.html

- - [ page-full-width.html

- - [ page-html-elements.html

- - [ page-left-sidebar.html

- - [ page-right-sidebar.html

- - [ services.html

- - [ single-doctor.html

- - [ single-news.html

- - [ single-services.html

- - [ ssi-footer.html

- - [ ssi-header.html

- - [ style.css

 

The HTML page structure:

.header - Site header it contains Toolbar, Logo and Main menu

.intro - Main Slider goes here

.main - Main Section

#section-book-appointment - Book an Appointment Section. 

#section-information - About Section.. 

#section-doctors - Section Doctors. 

#section-services - Latest services. 

.ad - Book now banner. 

.section-testimonials - Latest testimonials. 

.section-updates - Latest News. 

.footer - The footer of the site. It contains 3 widget area, one menu and the copyright of the site.

SASS and CSS Structure
SAAS

 - [ src 
 - - - - [ sass
 - - - - - - - [ layouts
 - - - - - - - - - - [  __layouts.scss
 - - - - - - - - - - [ _error.scss
 - - - - - - - - - - [ _footer.scss
 - - - - - - - - - - [ _header.scss
 - - - - - - - - - - [ _main.scss
 - - - - - - - - - - [ _sections.scss
 - - - - - - - - - - [ _sidebar.scss
 - - - - - - - - - - [ intro.scss
 - - - - - - - [ modules
 - - - - - - - - - - [ bxslider
 - - - - - - - - - - - - - [ jquery.bxslider.scss
  - - - - - - - - - - [ datepicker
 - - - - - - - - - - - - - [ foundation-datepicker.scss
 - - - - - - - - - - [ font-awesome
 - - - - - - - - - - [ __modules.scss
 - - - - - - - - - - [ _ad.scss
 - - - - - - - - - - [ _audio.scss
 - - - - - - - - - - [ _breadcrumbs.scss
 - - - - - - - - - - [ _buttons.scss
 - - - - - - - - - - [ _pager.scss
 - - - - - - - - - - [ _form-elements.scss
 - - - - - - - - - - [ _phone.scss
 - - - - - - - - - - [ _sliders.scss
 - - - - - - - - - - [ _socials.scss
 - - - - - - - - - - [ _subscribe.scss
 - - - - - - - - - - [ _tabs.scss
 - - - - - - - - - - [ mediaelementplayer.min.scss
 - - - - - - - - - - [ _search.scss
 - - - - - - [ _fonts.scss
 - - - - - - [ _global.scss
 - - - - - - [ _helpers.scss
 - - - - - - [ _icons-secondary.scss
 - - - - - - [ _icons.scss 
 - - - - - - [ _settings.scss
 - - - - - - [ _varialbe.scss
 - - - - - - [ _animate.scss
 - - - - - - [ style.scss
JavaScript
Here you will find the path to the all JavaScripts fiels.

- - [ assets
- - - - [ javascripts
- - - - - - - - - - [ app.js
- - - - - - - - - - [ vendor.js
- - - - - - - - - - [ jquery.stellar.min.js
PSD Files
I've included 9 psds with this theme:

doctor-profile.psd
doctors.psd
homepage.psd
homepage1.psd
news-detail.psd
news.psd
services-detail.psd
services.psd
services1.psd